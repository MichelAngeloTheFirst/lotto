package pl.akademiakodu.lotto;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.xml.ws.ResponseWrapper;

@Controller

public class HomeController {

    /* @ResponseBody, że rezultat będzie zwrócony będzie Stringiem
    */
    @ResponseBody
    @GetMapping("/bye")// po tej ścieżce wywołana jest metoda hello

    public String Hello(){
        return "Nara!";
    }


    @GetMapping("/welcome")
    public String welcome(){
        return "ELO"; //że ma zwrócić html:
        //resources/templates/ELO
    }

    @GetMapping("/produkt")
    public String product(){
        return "prod";
    }

    @GetMapping("/lotto")
    public String generalLotto(ModelMap map){

        LottoGenerator lottoGenerator=new LottoGenerator();
        map.put( "numbers", lottoGenerator.generate() );

        return "lotto";
    }

}
