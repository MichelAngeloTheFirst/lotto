package pl.akademiakodu.lotto;


import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

public class LottoGenerator {

    public Set<Integer> generate(){
        Set<Integer> set=new TreeSet<>();
        Random random=new Random();

        while(set.size()!=6){
            set.add(random.nextInt(49)+1);
        }
        return set;
    }

    public static void main(String[] args) {

    }
}
